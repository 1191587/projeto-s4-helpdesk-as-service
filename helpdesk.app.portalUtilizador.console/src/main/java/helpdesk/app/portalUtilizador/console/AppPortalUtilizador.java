package helpdesk.app.portalUtilizador.console;

import eapli.framework.infrastructure.eventpubsub.EventDispatcher;
import helpdesk.app.common.console.HelpDeskAppTemplate;
import helpdesk.app.portalUtilizador.console.presentation.FrontMenu;
import helpdesk.persistence.PersistenceContext;
import helpdesk.protocoloComunicacao.clients.MotorFluxoClient;
import helpdesk.usermanager.domain.BasePasswordPolicy;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;
import eapli.framework.infrastructure.authz.domain.model.PlainTextEncoder;


@SuppressWarnings("squid:S106")
public final class AppPortalUtilizador extends HelpDeskAppTemplate {

    public static void main(String[] args){
        AuthzRegistry.configure(PersistenceContext.repositories().users(),
                new BasePasswordPolicy(), new PlainTextEncoder());



//        BaseBootstrap.main(new String[]{"-embedded"});

        new AppPortalUtilizador().run(args);
    }

    @Override
    protected void doMain(String[] args) {
        new FrontMenu().show();
    }

    @Override
    protected String appTitle() {
        return "Aplicação HelpDesk - Portal do Utilizador";
    }

    @Override
    protected void doSetupEventHandlers(EventDispatcher dispatcher) {

    }


}
