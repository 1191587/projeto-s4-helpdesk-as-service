# README #



     _    _          _               _                _                        _____                         _               
    | |  | |        | |             | |              | |        /\            / ____|                       (_)              
    | |__| |   ___  | |  _ __     __| |   ___   ___  | | __    /  \     ___  | (___     ___   _ __  __   __  _    ___    ___
    |  __  |  / _ \ | | | '_ \   / _` |  / _ \ / __| | |/ /   / /\ \   / __|  \___ \   / _ \ | '__| \ \ / / | |  / __|  / _ \
    | |  | | |  __/ | | | |_) | | (_| | |  __/ \__ \ |   <   / ____ \  \__ \  ____) | |  __/ | |     \ V /  | | | (__  |  __/
    |_|  |_|  \___| |_| | .__/   \__,_|  \___| |___/ |_|\_\ /_/    \_\ |___/ |_____/   \___| |_|      \_/   |_|  \___|  \___|
                        | |                                                                                                  
                        |_|

(logo created with [kammerl ascii signature](https://www.kammerl.de/ascii/AsciiSignature.php))



## License and copyright ##

Copyright (c) 2013-2021 the original author or authors.

MIT License

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.




##  System Overview ##

![applications](docs/aplicações_preconizadas.png)


### Brief [Powerpoint presentation](docs/Presentation.pptx) of the project and initial development. ###

###  Also, check the [docs](docs/README.md) folder for the full documentation on the use cases developed so far. ###



## Bulding and Running the Application ##

Project configured to run locally.

All scrips necessary are in the [scripts](./scripts) folder. Use .sh or .bat files according to the operating system in use.

Deploy order:

    1. build-all/rebuild-all
    2. run-executorTarefas
    3. run-motorFluxo
    4. (optional) run-bootstrap (sample data)
    4. run-portalUtilizador and/or run-servicosERH

